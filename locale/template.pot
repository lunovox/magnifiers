# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-07 09:51-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: init.lua:12 init.lua:96 init.lua:126
msgid "Object Dates"
msgstr ""

#: init.lua:17
msgid "CLOSE"
msgstr ""

#: init.lua:50
msgid "Magnifier"
msgstr ""

#: init.lua:51
msgid "Displays item description on the map."
msgstr ""

#: init.lua:69
msgid "You are pointing to nothing parsable!"
msgstr ""

#: init.lua:80
msgid "This is the '@1' player!"
msgstr ""

#: init.lua:99 init.lua:106
msgid "You are pointing to '@1'!"
msgstr ""

#: init.lua:103
msgid "You are pointing at any item!"
msgstr ""

#: init.lua:133
msgid "This is '@1'. [@2]"
msgstr ""

#: init.lua:142
msgid "That's '@1'"
msgstr ""

#: init.lua:148
msgid "Lighting: @1%"
msgstr ""

#: init.lua:151
msgid "The '@1' block is an unknown item from a disabled mod!"
msgstr ""
