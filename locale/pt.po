# Portuguese translations for PACKAGE package
# Traduções em português brasileiro para o pacote PACKAGE.
# Copyright (C) 2024 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-07 09:51-0300\n"
"PO-Revision-Date: 2024-03-07 09:55-0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Lunovox Heavenfinder\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: init.lua:12 init.lua:96 init.lua:126
msgid "Object Dates"
msgstr "Dados do Objeto"

#: init.lua:17
msgid "CLOSE"
msgstr "FECHAR"

#: init.lua:50
msgid "Magnifier"
msgstr "Lupa"

#: init.lua:51
msgid "Displays item description on the map."
msgstr "Exibe a descrição do item no mapa."

#: init.lua:69
msgid "You are pointing to nothing parsable!"
msgstr "Você não está apontando para nada analisável!"

#: init.lua:80
msgid "This is the '@1' player!"
msgstr "Este é o(a) jogador(a) '@1'!"

#: init.lua:99 init.lua:106
msgid "You are pointing to '@1'!"
msgstr "Você está apontando para '@1'!"

#: init.lua:103
msgid "You are pointing at any item!"
msgstr "Você está apontando para um item qualquer!"

#: init.lua:133
msgid "This is '@1'. [@2]"
msgstr "Este é '@1'. [@2]"

#: init.lua:142
msgid "That's '@1'"
msgstr "Isso é '@1'"

#: init.lua:148
msgid "Lighting: @1%"
msgstr "Iluminação: @1%"

#: init.lua:151
msgid "The '@1' block is an unknown item from a disabled mod!"
msgstr "O bloco '@1' é um item desconhecido de um mod desabilitado!"
