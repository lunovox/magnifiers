modMgnifiers = { 
	showFormText = function(playername, texto)
		if type(texto)=="string" and texto ~= "" then
			local myFormSpec = ""
			myFormSpec = myFormSpec
			.."formspec_version[6]"
			.."size[16,8,false]"
			--.."background[0,-8;16,16;text_eurn_front.png]"
			.."box[0.5,0.75;15,5.75;#000000CC]"
			.."textarea[0.5,0.75;15,5.75;;"
				..minetest.formspec_escape(
					modMgnifiers.translate("Object Dates")..": "..
					core.colorize("#00FF00", playername)
				)..";"
				..minetest.formspec_escape(texto)..
			"]"
			.."button_exit[12.85,6.75;2.55,1;;"..modMgnifiers.translate("CLOSE").."]"
			minetest.show_formspec(playername, "frmDescription", myFormSpec)
		end
	end,
}

--##############################################################################

local ngettext

--[[
local S = minetest.get_translator('testmod')
minetest.register_craftitem('testmod:test', {
    description = S('I am a test object'),
    inventory_image = 'default_stick.png^[brighten'
})
--]]

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		modMgnifiers.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		modMgnifiers.translate = intllib.Getter()
	end
elseif minetest.get_translator ~= nil and minetest.get_current_modname ~= nil and minetest.get_modpath(minetest.get_current_modname()) then
	modMgnifiers.translate = minetest.get_translator(minetest.get_current_modname())
else
	modMgnifiers.translate = function(s) return s end
end

--##############################################################################

minetest.register_craftitem("magnifiers:magnifier", {
	description = modMgnifiers.translate("Magnifier").."\n"
	.." * "..modMgnifiers.translate("Displays item description on the map."),
	image = "obj_lupa.png",
	groups = {tool=1, glass=1, magnifier=1},
	liquids_pointable = true, -- Pode apontar para node de Liquidos (mais tarde podera coletar agua)
	stack_max=1, --Acumula 1 por slot
	on_use = function(itemstack, user, pointed_thing)
		--[[
			minetest.registered_entities["__builtin:item"].selectionbox = {0,0,0,0,0,0}
			minetest.registered_entities["__builtin:item"].initial_properties.selectionbox = {0,0,0,0,0,0}
			minetest.registered_entities["__builtin:item"].pointable = false
			fonte: https://forum.minetest.net/viewtopic.php?f=47&t=22771
		--]]
		
		local username = user:get_player_name()
		--minetest.chat_send_player(username, "bbbb"..dump(pointed_thing))
		
		if type(pointed_thing)~="nil" and type(pointed_thing.type)~="nil"then
			if pointed_thing.type == "nothing" then
				minetest.chat_send_player(username, modMgnifiers.translate("You are pointing to nothing parsable!"))
			elseif pointed_thing.type == "object" then
				--minetest.chat_send_player(username, "cccc1"..dump(pointed_thing.ref))
				local pos = minetest.get_pointed_thing_position(pointed_thing, true)
				--minetest.chat_send_player(username, "cccc2"..dump(pos))
				local objs = minetest.get_objects_inside_radius(pos, 1)
				for _, obj in pairs(objs) do
					if type(obj)~="nil" then
						--minetest.registered_entities[mob]
						if obj:is_player() then --SE OBJETO FOR UM JOGADOR
							local playername = obj:get_player_name()
							minetest.chat_send_player(username, modMgnifiers.translate("This is the '@1' player!", minetest.colorize("#00FF00", playername)))
						elseif type(obj:get_luaentity())~="nil" and type(obj:get_luaentity().name)~="nil" then --SE OBJETO FOR UMA ENTIDADE
							local ent = obj:get_luaentity()
							if ent.name=="__builtin:item" then
								if type(ent.itemstring)~="nil" and ent.itemstring~="" then
									local partes_name = ent.itemstring:split(" ")
									if #partes_name > 0 then
										if minetest.get_player_privs(username)["server"] then
											--[[
											minetest.chat_send_player(username, "###### '"..minetest.colorize("#00FF00", partes_name[1]).."' ######")
											minetest.chat_send_player(username, dump(ent))
											minetest.chat_send_player(username, "Light="..minetest.get_node_light(pos, nil))
											--]]
											local texto = "###### '"..minetest.colorize("#00FF00", partes_name[1]).."' ######\n"
											..dump(ent).."\n"
											.."Light="..minetest.get_node_light(pos, nil)
											minetest.log("action", "[MAGNIFIERS:"..username:upper()..":"..minetest.pos_to_string(pos, 0).."] "..modMgnifiers.translate("Object Dates")..": \n"..texto)
											modMgnifiers.showFormText(username, texto)--modMgnifiers.showFormText(playername, texto)
										else
											minetest.chat_send_player(username, modMgnifiers.translate("You are pointing to '@1'!", minetest.colorize("#00FF00", partes_name[1])))
										end
									end
								else
									minetest.chat_send_player(username, modMgnifiers.translate("You are pointing at any item!"))
								end
							else
								minetest.chat_send_player(username, modMgnifiers.translate("You are pointing to '@1'!", minetest.colorize("#FFFF00", ent.name)))
							end
						end
					end
				end
			elseif pointed_thing.type == "node" and type(pointed_thing.under)~="nil" then --SE OBJETO FOR UMA NODE
				node = minetest.get_node(pointed_thing.under)
				props = minetest.registered_items[node.name]
				if type(props)~="nil" then
					if minetest.get_player_privs(username)["server"] then
						local node = minetest.get_node(pointed_thing.under)
						props = minetest.registered_items[node.name]
						--[[
						minetest.chat_send_player(username, "###### '"..minetest.colorize("#FFFF00", node.name).."' ########################################################################")
						minetest.chat_send_player(username, minetest.colorize("#CCCCCC", dump(props)))
						minetest.chat_send_player(username, "Light="..minetest.colorize(minetest.get_node_light(pointed_thing.above, nil)))
						--]]
						local texto = "###### '"..minetest.colorize("#FFFF00", node.name).."' ######\n"
						..dump(props).."\n"
						.."Light="..minetest.get_node_light(pointed_thing.above, nil)
						minetest.log("action", "[MAGNIFIERS:"..username:upper()..":"..minetest.pos_to_string(pointed_thing.above, 0).."] "..modMgnifiers.translate("Object Dates")..": \n"..texto)  
						modMgnifiers.showFormText(username, texto) --modMgnifiers.showFormText(playername, texto)
					else
						if props.description~="" then
							minetest.chat_send_player(
								username, 
								modMgnifiers.translate(
									"This is '@1'. [@2]",
									minetest.colorize("#00FF00", props.description),
									minetest.colorize("#FFFF00", node.name)
								)
							)
						else
							minetest.chat_send_player(
								username, 
								modMgnifiers.translate(
									"That's '@1'", 
									minetest.colorize("#FFFF00", node.name)
								)
							)
						end
						local iluminacao = math.floor((minetest.get_node_light(pointed_thing.above, nil)/15)*100)
						minetest.chat_send_player(username, modMgnifiers.translate("Lighting: @1%", minetest.colorize("#8888FF", iluminacao)))
					end
				else
					minetest.chat_send_player(username, modMgnifiers.translate("The '@1' block is an unknown item from a disabled mod!", node.name))
				end
			end
		end
	end,
})

minetest.register_craft({
	output = "magnifiers:magnifier",
	recipe = {
		{"",					"",					"default:glass"},
		{"",					"default:stick",	""},
		{"default:stick",	"",					""},
	},
})

minetest.register_alias("magnifier"	, "magnifiers:magnifier")
minetest.register_alias("lupa"		, "magnifiers:magnifier")
