![screenshot]
# [MAGNIFIERS]

[![minetest_icon]][minetest_link] [![baixa_icon]][baixa_link] [![projeto_icon]][projeto_link] <!-- [![bower_icon]][bower_link] -->  

Adds a magnifying glass that displays the item's properties for those who have the 'server' privilege.

## **License:**

* [![license_icon]][license_link]

 More details: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

## **Downloads:**

* [Repository]
* [Stable Releases]

## **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [xmpp](xmpp:lunovox@disroot.org?join), [social web](http://qoto.org/@lunovox), [audio conference](https://meet.jit.si/MinetestBrasil), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

## 📦 **Depends:**

| Mod Name | Dependency Type | Descryption |
| :--: | :--: | :-- |
| [default][Minetest Game] | Mandatory |  [Minetest Game] Included. | 

## **INTERNACIONALIZATION:**

### **Available Languages:**

* [English] (Defaul, Concluded: 100%)
* [Portuguese] (Concluded: 100%)
* [Brazilian Portuguese] (Concluded: 100%)

### **Translate this mod to your Language:**

Please help translate this mod into your native language. See more details in file: [locale/README.md]

[screenshot]:screenshot.png
[MAGNIFIERS]:https://gitlab.com/lunovox/magnifiers/
[baixa_icon]:https://img.shields.io/static/v1?label=Download&message=Mod&color=blue
[baixa_link]:https://gitlab.com/lunovox/magnifiers/-/archive/master/magnifiers-master.zip?inline=false
[bower_icon]:https://img.shields.io/badge/Bower-Projeto-green.svg
[bower_link]:https://gitlab.com/lunovox/magnifiers/-/raw/master/textures/bower.json
[GNU AGPL-3.0]:https://gitlab.com/lunovox/magnifiers/-/raw/master/LICENSE
[license_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=Download&color=yellow
[license_link]:https://gitlab.com/lunovox/magnifiers/-/raw/master/LICENSE
[locale/README.md]:https://gitlab.com/lunovox/magnifiers/-/tree/master/locale?ref_type=heads
[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Game&color=brightgreen
[minetest_link]:https://minetest.net
[Minetest Game]:https://content.minetest.net/packages/Minetest/minetest_game/
[projeto_icon]:https://img.shields.io/static/v1?label=Projeto&message=GIT&color=red
[projeto_link]:https://gitlab.com/lunovox/magnifiers
[Repository]:https://gitlab.com/lunovox/magnifiers/
[Stable Releases]:https://gitlab.com/lunovox/magnifiers/-/tags
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

[English]:https://gitlab.com/lunovox/magnifiers/-/blob/master/locale/template.pot?ref_type=heads
[Portuguese]:https://gitlab.com/lunovox/magnifiers/-/blob/master/locale/pt.po?ref_type=heads
[Brazilian Portuguese]:https://gitlab.com/lunovox/magnifiers/-/blob/master/locale/pt_BR.po?ref_type=heads



